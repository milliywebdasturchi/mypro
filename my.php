<?php 

if(isset($_POST['send'])){
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $age = $_POST['age'];
    $color = $_POST['color'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php</title>
    <link rel="stylesheet" href="style.css">
</head>
<body style="background: <?=$color;?>">
    <div class="wrap">
        <h3>Hello,  <?=$firstName;?></h3>
        <p>Your fullname: <?=$firstName;?> <?=$lastName;?></p>
        <p>Your age: <?=$age;?></p>
        <p>Your color: <?=$color;?></p>
        <p><a href="index.php">Home back</a></p>
    </div>
</body>
</html>