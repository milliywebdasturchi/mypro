<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>php</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="wrap">
        <form action="my.php" method="post">
            <label for="firstName">Firstname</label>
            <input type="text" name="firstName" id="firstName" placeholder="Enter firstname">
            <label for="lastName">Lastname</label>
            <input type="text" name="lastName" id="lastName" placeholder="Enter lastname">
            <label for="age">Age</label>
            <input type="text" name="age" id="age" placeholder="Enter age">
            <label for="color">Color</label>
            <input type="text" name="color" id="color" placeholder="Enter color name">
            <button type="submit" name="send">Send</button>
        </form>
    </div>
</body>
</html>